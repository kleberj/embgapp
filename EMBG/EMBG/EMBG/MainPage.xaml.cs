﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EMBG
{
    public partial class MainPage : ContentPage, ISession
    {
        public MainPage()
        {
            InitializeComponent();
            Session.Register(this);
        }

        private void Entry_Completed(object sender, EventArgs e)
        {
            //hole Zahlen aus Eingabestring
            string s =  extract_digits(SN_Entry.Text);
            //zeige Alert und ersetze Eingabe, falls s von Eingabe abweicht (-> ungültige Zeichen)
            if(s!=SN_Entry.Text.Trim())
            {
                DisplayAlert("Unerlaubte Zeichen!", "Deine Schülernummer enthält nicht nur Zahlen! Bitte überprüfe die geänderte Eingabe!", "Verstanden");
                SN_Entry.Text = s;
            }
            // setze neuen BarcodeValue, falls Entry-Eintrag komplett
            // und ungleich Leerstring
            if (SN_Entry.Text.Trim() != "")
                bcimgview.BarcodeValue = SN_Entry.Text.Trim();
        }

        // extrahiere nur die Zahlen aus einem String
        private string extract_digits(string s)
        {
            string result = string.Empty;
            foreach (var c in s)
            {
                int ascii = (int)c;
                if (ascii >= 48 && ascii <= 57)
                    result += c;
            }
            return result;
        }


        // speichere aktuelle Schülernummer (Property)
        public void Save()
        {
            Application.Current.Properties["embg_snr"] = SN_Entry.Text;
        }

        // zuletzt verwendete Schülernummer (Property) laden
        public void Restore()
        {
            string s;
            if (Application.Current.Properties.ContainsKey("embg_snr"))
            {
                s = (string)Application.Current.Properties["embg_snr"];
                if (s == "")
                    s = "123456";
            }
            else
                s = "123456";

            SN_Entry.Text = s;
            bcimgview.BarcodeValue = s;
        }
    }
}
