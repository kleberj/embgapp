﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace EMBG
{
    public interface ISession
    {
        void Save();
        void Restore();
    }

    class Session
    {
        static List<ISession> sessions = new List<ISession>();

        static public void Register(ISession session)
        {
            sessions.Add(session);
        }

        static public void UnRegister(ISession session)
        {
            sessions.Remove(session);
        }
        static public void Save()
        {
            foreach (var session in sessions)
                session.Save();
        }

        static public void Restore()
        {
            foreach (var session in sessions)
                session.Restore();
        }

        static public void Save(object dataObject)
        {
            string json = JsonConvert.SerializeObject(dataObject);
            string key = dataObject.GetType().ToString();
            Application.Current.Properties[key] = json;
        }

        static public object Restore(Type typeObject)
        {
            string key = typeObject.ToString();

            if (!Application.Current.Properties.ContainsKey(key))
                return null;

            string json = (string)Application.Current.Properties[key];
            return JsonConvert.DeserializeObject(json, typeObject);
        }
    }
}
