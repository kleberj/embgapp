﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace EMBG
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            Session.Restore();
        }

        protected override void OnSleep()
        {
            Session.Save();
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
